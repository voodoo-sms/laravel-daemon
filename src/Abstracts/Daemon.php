<?php

namespace VoodooSMS\LaravelDaemon\Abstracts;

use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Console\Command;
use Illuminate\Contracts\Foundation\Application;
use Throwable;
use VoodooSMS\LaravelDaemon\Exceptions\WorkMethodMissingException;
use VoodooSMS\LaravelDaemon\Interfaces\DaemonManagerInterface;

abstract class Daemon extends Command
{
    /**
     * Whether the loop should run.
     */
    protected bool $run = true;

    /**
     * Whether the work is complete.
     */
    protected bool $complete = false;

    /**
     * The number of seconds to sleep for.
     */
    protected float $sleep = 1;

    /**
     * The iteration number.
     */
    protected int $runs = 1;

    /**
     * Whether the work has started or not.
     */
    protected bool $started = false;

    /**
     * Unique id of the instance.
     */
    protected string $instanceId;

    /**
     * Unique id of the current iteration.
     */
    protected string $batchId;

    private Application $app;

    private DaemonManagerInterface $manager;

    public function __construct(Application $app, DaemonManagerInterface $manager)
    {
        $this->app = $app;
        $this->manager = $manager;
        $this->signature .= ' {--once} {--sleep=-1} {--until-complete}';

        if (!method_exists($this, 'work')) {
            throw new WorkMethodMissingException;
        }

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getInstanceId();
        $this->listenForSignals();
        $this->sleep = $this->getSleep();

        try {
            $this->setUp();
        } catch (Throwable $t) {
            $this->error('Something went wrong during setUp().');
            $this->error($t->getMessage());

            return 1;
        }

        $this->printInfo();
        $this->started = true;

        while ($this->run) {
            if ($this->manager->shouldRestart()) {
                $this->info('Detected daemon restart signal.');
                $this->shutdown();
                continue;
            }

            $this->getBatchId();

            $this->app->call([$this, 'work']);

            unset($this->batchId);

            if (!$this->shouldContinue()) {
                $this->run = false;
                continue;
            }

            $this->runs++;
            $this->sleep();
        }

        try {
            $this->tearDown();
        } catch (Throwable $t) {
            $this->error('Something went wrong during tearDown().');
            $this->error($t->getMessage());

            return 1;
        }

        $this->line('Stopping daemon');
    }

    /**
     * Print startup info to the console.
     *
     * @return void
     */
    protected function printInfo(): void
    {
        $this->line('Starting daemon');

        if ($this->shouldRunOnce()) {
            $this->line('Note: running once due to the --once flag');
        }

        if ($this->shouldRunUntilComplete() && !$this->shouldRunOnce()) {
            $this->line('Note: running until marked as complete due to the --until-complete flag');
        }
    }

    /**
     * Sleep at the end of the loop.
     *
     * @return void
     */
    protected function sleep(): void
    {
        usleep($this->sleep);
    }

    protected function shouldContinue(): bool
    {
        if ($this->shouldRunOnce() && $this->runs === 1) {
            return false;
        }

        if ($this->shouldRunUntilComplete() && $this->complete) {
            return false;
        }

        return true;
    }

    /**
     * Determine if the loop should only run once.
     *
     * @return bool
     */
    private function shouldRunOnce(): bool
    {
        if ($this->option('once') === true) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the loop should run until complete.
     *
     * @return bool
     */
    private function shouldRunUntilComplete(): bool
    {
        if ($this->option('until-complete') === true) {
            return true;
        }

        return false;
    }

    /**
     * Mark the work as completed.
     *
     * @return void
     */
    protected function completed(): void
    {
        $this->complete = true;
    }

    /**
     * Listen for system signals.
     *
     * @return void
     */
    protected function listenForSignals()
    {
        pcntl_async_signals(true);

        pcntl_signal(SIGINT, [$this, 'shutdown']);
        pcntl_signal(SIGTERM, [$this, 'shutdown']);
    }

    /**
     * Shutdown gracefully
     *
     * @return void
     */
    public function shutdown(): void
    {
        $this->run = false;
    }

    /**
     * Get the amount of time to sleep in seconds.
     *
     * @return int
     */
    private function getSleep(): float
    {
        $sleep = (float) $this->option('sleep');

        $sleep = $sleep == -1
            ? config('daemon.sleep')
            : $sleep;

        return $sleep * 1000000;
    }

    /**
     * Write a string as standard output.
     *
     * @param  string  $string
     * @param  string|null  $style
     * @param  int|string|null  $verbosity
     * @return void
     */
    public function line($string, $style = null, $verbosity = null)
    {
        $formatted = Carbon::now()->format('[Y-m-d H:i:s.v] - ') . $this->getInstanceId();

        if ($this->started && $this->run) {
            $formatted .= ':' . $this->getBatchId();
        }

        $formatted .= ' - ' . $string;

        parent::line($formatted, $style, $verbosity);
    }

    /**
     * Return the instance id of the daemon.
     *
     * @return string
     */
    protected function getInstanceId(): string
    {
        if (!isset($this->instanceId)) {
            $td = gettimeofday();
            $this->instanceId =  (new Hashids)->encode($td['sec'] + $td['usec']);
        }

        return $this->instanceId;
    }

    /**
     * Get the batch id.
     *
     * @return string
     */
    protected function getBatchId(): string
    {
        if (!isset($this->batchId)) {
            $this->batchId = substr(
                md5($this->getInstanceId() . ':' . $this->runs),
                0,
                7
            );
        }

        return $this->batchId;
    }

    /**
     * Command setup to run before the work loop starts.
     *
     * @return void
     */
    public function setUp(): void
    {
        //
    }

    /**
     * Work to do after the loop ends.
     *
     * @return void
     */
    public function tearDown(): void
    {
        //
    }
}
