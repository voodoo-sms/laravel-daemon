<?php

namespace VoodooSMS\LaravelDaemon;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\ServiceProvider;
use VoodooSMS\LaravelDaemon\Commands\DaemonGenerator;
use VoodooSMS\LaravelDaemon\Commands\RestartDaemon;
use VoodooSMS\LaravelDaemon\Interfaces\DaemonManagerInterface;
use VoodooSMS\LaravelDaemon\Utils\DaemonManager;

class LaravelDaemonServiceProvider extends ServiceProvider
{
    private static array $commands = [
        DaemonGenerator::class,
        RestartDaemon::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/Config/daemon.php', 'daemon');

        $this->app->bind(DaemonManagerInterface::class, function () {
            return new DaemonManager(
                app()->make(Repository::class)
            );
        });
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands(self::$commands);

            $this->publishes([
                __DIR__ . '/Config/daemon.php' => config_path('daemon.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/Stubs/' => resource_path('stubs/'),
            ], 'stubs');
        }
    }
}
